const http = require('http');
const path = require('path');

const express = require('express');

const app =express();
const server = http.createServer(app);
const io = require('socket.io').listen(server);


//settings
app.set('port', process.env.PORT || 3000);
require ('./sockets')(io);



//static files
//console.log(__dirname)
console.log(path.join(__dirname, 'public'));
app.use(express.static(path.join(__dirname, 'public')));

//empezando el servidor

server.listen(app.get('port'), ()=>{
    console.log('server on port ', app.get ('port'));
});
